import React, { Component } from "react";
import Card from "../card/Card";
import styles from "./Products.module.scss";
import PropTypes from "prop-types";

class Products extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className={styles.ProductsList}>
        {this.props.products.map((product) => (
          <>
            {this.props.cart ? localStorage.setItem("cart", product) : null}
            <Card
              imgSrc={product.image}
              title={product.name}
              color={product.color}
              price={product.price}
              key={product.name}
              onClick={this.props.onClick}
            ></Card>
          </>
        ))}
      </div>
    );
  }
}

export default Products;
