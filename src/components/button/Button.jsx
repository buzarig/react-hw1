import { Component } from "react";
import styles from "./Button.module.scss";
import PropTypes from "prop-types";

class Button extends Component {
  render() {
    return (
      <button
        className={styles.Button}
        style={{ background: this.props.background }}
        onClick={this.props.onClick}
        id={this.props.modalId}
      >
        {this.props.text}
      </button>
    );
  }
}

Button.propTypes = {
  background: PropTypes.string,
  onClick: PropTypes.func,
  text: PropTypes.string,
};

Button.defaultProps = {
  background: "wheat",
};

export default Button;
