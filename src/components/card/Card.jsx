import React, { Component } from "react";
import styles from "./Card.module.scss";
import Button from "../button/Button";
import { Icon } from "@iconify/react";

class Card extends Component {
  constructor(props) {
    super(props);
    this.state = { icon: "bi:star" };
  }

  fillIcon = () => {
    this.setState({ icon: "bi:star-fill" });
  };

  render() {
    return (
      <div className={styles.Card}>
        <div>
          <img className={styles.CardImage} src={this.props.imgSrc}></img>
        </div>
        <div className={styles.CardContent}>
          <h1 className={styles.CardTitle}>{this.props.title}</h1>
          <Icon onClick={this.fillIcon} icon={this.state.icon} />
          <p>{this.props.color}</p>
          <span className={styles.CardPrice}>${this.props.price}</span>
          <Button
            onClick={this.props.onClick}
            text="ADD TO CARD"
            background="black"
          ></Button>
        </div>
      </div>
    );
  }
}

export default Card;
