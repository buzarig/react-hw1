const modalWindowDeclarations = [
  {
    modalId: "modalID1",
    modalProps: {
      title: "title for modal 1",
      description: "description for modal 1",
      background: "white",
      closeButton: true,
      actions: [
        {
          text: "No",
          background: "red",
          type: "cancel",
        },
        {
          text: "Yes",
          background: "green",
          type: "submit",
        },
      ],
    },
  },
  {
    modalId: "modalID2",
    modalProps: {
      title: "title for modal 2",
      description: "description for modal 2",
      background: "yellow",
      closeButton: false,
      actions: [
        {
          text: "No",
          background: "red",
          type: "cancel",
        },
        {
          text: "Yes",
          background: "green",
          type: "submit",
        },
      ],
    },
  },
];

export default modalWindowDeclarations;
