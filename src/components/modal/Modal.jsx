import React, { Component } from "react";
import { act } from "react-dom/test-utils";
import Button from "../button/Button";
import styles from "./Modal.module.scss";
import modalSettings from "./modalSettings";

class Modal extends Component {
  constructor(props) {
    super(props);
    this.state = { settings: {} };
  }

  componentDidMount() {
    const modalDeclaration = modalSettings.find(
      (item) => item.modalId === this.props.modalId
    );
    console.log(modalDeclaration);
    this.setState({ settings: modalDeclaration.modalProps });
  }

  render() {
    const { title, description, background, closeButton, actions } =
      this.state.settings;
    return (
      <div
        className={styles.Wrapper}
        onClick={(evt) =>
          evt.currentTarget === evt.target && this.props.close()
        }
      >
        <div style={{ background: background }} className={styles.Modal}>
          <div className={styles.Header}>
            {title}
            {closeButton && (
              <Button
                onClick={this.props.close}
                text="x"
                background="blue"
              ></Button>
            )}
          </div>
          <div className={styles.Text}>{description}</div>
          <div className={styles.Actions}>
            {actions &&
              actions.map((action) => (
                <Button
                  background={action.background}
                  text={action.text}
                  key={action.type}
                  onClick={
                    action.type === "submit"
                      ? () => {
                          console.log("ooldooal");
                        }
                      : this.props.close
                  }
                />
              ))}
          </div>
        </div>
      </div>
    );
  }
}

export default Modal;
