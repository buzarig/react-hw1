import "./App.css";
import Products from "./components/products/Products";
import React, { Component } from "react";
import Button from "./components/button/Button";
import Modal from "./components/modal/Modal";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
      modal: {
        showModal: false,
        modalId: null,
      },
    };
  }

  // componentDidMount() {
  //   fetch("/products.json")
  //     .then((response) => response.json())
  //     .then((products) => {
  //       this.setState({ products });
  //     });
  // }

  handleOpenModal = (modalId) => {
    this.setState({
      modal: {
        showModal: true,
        modalId,
      },
    });
  };

  handleCloseModal = () => {
    this.setState({
      modal: {
        showModal: false,
        modalId: null,
      },
    });
  };

  render() {
    return (
      <>
        <header className="header"></header>

        <Button
          background="green"
          text="open first modal"
          onClick={() => {
            this.handleOpenModal("modalID1");
          }}
        />
        <Button
          background="red"
          text="open second modal"
          onClick={() => {
            this.handleOpenModal("modalID2");
          }}
        />
        {this.state.modal.showModal && (
          <Modal
            close={this.handleCloseModal}
            modalId={this.state.modal.modalId}
          />
        )}
      </>
    );
  }
}

export default App;
